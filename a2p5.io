math := Object clone
math fibonacci := method(x,
  if(x == 0, return 0)
  if(x == 1, return 1)
  if(x == 2, return 1)
  return fibonacci(x - 1) + fibonacci(x - 2)

)

"Enter a number you would like to find" println
num := File standardInput readLine
num = num asNumber

math fibonacci(num) print
" is the fibonacci number" println
