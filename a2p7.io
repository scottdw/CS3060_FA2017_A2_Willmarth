size := 50
aMatrix := List clone
bMatrix := List clone
cMatrix := List clone

//A for-loop that creates a list where each list is a row in the matrix
for(x,0,size-1,
  aList := List clone
  size repeat(aList append(Random value(size) floor))
  aMatrix append(aList)
)

aMatrix println

//Same thing as above but for matrix b
for(x,0,size-1,
  bList := List clone
  size repeat(bList append(Random value(size) floor))
  bMatrix append(bList)
)

bMatrix println

for(i,0,size-1,
  cList := List clone
  for(j,0,size-1,
    sum := 0
    for(k,0,size-1,
      aVal := aMatrix at(i) at(k)
      bVal := bMatrix at(k) at(j)
      sum = sum +  aVal * bVal
    )
    cList append(sum)
  )
  cMatrix append(cList)
)
"The product " println
"-----------------------------------------------------" println
cMatrix println
