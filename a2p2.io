"Enter a string you would like to use" println
theString := File standardInput readLine
"Enter a substring you would like to search for" println
subString := File standardInput readLine

if(theString containsSeq(subString),
  subSize := subString size
  strSize := theString size
  theBeg := 0
  theEnd := subSize
  theIndex := -1
  while(theEnd < strSize,
      theString exSlice(theBeg, theEnd) println
      if(theString exSlice(theBeg, theEnd) == subString) then(theIndex := theBeg)
      theBeg := theBeg + 1
      theEnd := theEnd + 1
  )
  theIndex println
)
