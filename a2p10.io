Shape := Object clone
Shape color := "red"
Shape filled := true
Shape default := method(self color = " ", self filled = false)
Shape getColor := method(return color)
Shape setColor := method(c, color := c)
Shape isFilled := method(return filled)
Shape setFilled := method(f, filled := f)
Shape overload := method(c,f,
self color := c
self filled := f
)
Shape toString := method()

Circle := Shape clone
Circle radius := 1.0
Circle default := method(self radius = 1.0, self color = " ", self filled = false)
Circle setRadius := method(r, radius := r)
Circle getRadius := method(return radius)
Circle getArea := method(return (radius * radius * 3.14))
Circle getPerimeter := method(return (radius * 2 * 3.14))
Circle overload1 := method(r, self radius = r)
Circle overload2 := method(c,f,r,
self color = c
self filled = f
self radius = r
)

Rectangle := Shape clone
Rectangle width := 1.0
Rectangle length := 1.0
Rectangle default := method(self color = " ", self filled = false, self width = 0.0, self length = 0.0)
Rectangle setWidth := method(w, width := w)
Rectangle getWidth := method(return width)
Rectangle setLength := method(l, length := l)
Rectangle getLength := method(return length)
Rectangle overload1 := method(w,l,
self width = w
self length = l
)
Rectangle overload2 := method(c,f,w,l,
self color = c
self filled = f
self width = w
self length = l
)

Square := Rectangle clone
Square side := 1.0
Square default := method(self color = " ", self filled = false, self side = 0.0)
Square overload1 := method(s, self side = s)
Square overload2 := method(c,f,s,
self color = c
self filled = f
self side = s
)
Square setSide := method(s, side := s)
Square getSide := method(return side)

Shape getColor println
Shape isFilled println
Shape color = Shape setColor("green")
Shape filled = Shape setFilled(false)
Shape getColor println
Shape isFilled println
Shape overload("Yellow", true)
Shape getColor println
Shape isFilled println

Circle getRadius println
Circle radius = Circle setRadius(3.0)
Circle getRadius println
Circle overload1(7.0)
Circle getRadius println

Rectangle getWidth println
Rectangle getLength println
Rectangle width = Rectangle setWidth(2.0)
Rectangle length = Rectangle setLength(3.0)
Rectangle getWidth println
Rectangle getLength println

Rectangle overload1(4.0,3.2)
Rectangle getColor println
Rectangle isFilled println
Rectangle getWidth println
Rectangle getLength println
