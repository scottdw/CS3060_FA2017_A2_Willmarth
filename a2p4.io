num := 100
array := List clone
num repeat(array append(Random value(num) round))

"The List" println
array println

for(i,0,array size,
  minimum := i
  for(h, i+1, array size, if(array at(h) < array at(minimum), minimum := h))
  if(minimum != i,
    newList := List clone
    for(x, 0, array size-1,
      if(x == minimum) then(newList append(array at(i))) elseif(x == i) then(newList append(array at(minimum))) else(newList append(array at(x)))
    )
    array = newList
  )
)
"The Final List"
array println
