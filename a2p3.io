array := List clone
num := 100
num repeat(array append(Random value(num) floor))

array := array sort
"Sorted Array" println
array println

array := array reverse
"Reverse Array" println
array println

for(i,0,num / 2, array remove(i * 2))
"Odd Index Only Array" println
array println
