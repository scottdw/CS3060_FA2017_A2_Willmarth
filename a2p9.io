csVRow := Object clone
csVRow read := method(file := File with("test.txt"))
csVRow display := method(
  file := self read
  file openForReading
  line := file readLine
  while(line != nil,
    line println
    line := file readLine
    )

    file close
)

csVRow display
