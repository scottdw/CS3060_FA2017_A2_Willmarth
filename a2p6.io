math := Object clone
math fibonacci := method(x,
  if(x == 0, return 0)
  if(x == 1, return 1)
  if(x == 2, return 1)
  return fibonacci(x - 1) + fibonacci(x - 2)

)

num := 500
for(i,0,num, math fibonacci(i) println)
