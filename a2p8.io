file := File with("filename")

split := file findSeq(",")
string1 := file exSlice(0,split)
string2 := file exSlice(split+1,file size)
hamD := 0

for(i,0,string1 size, if(string1 at(i) != string2 at(i), hamD = hamD + 1))

hamS := hamD asString

output := "#{string1}:#{string2}:#{hamS}" interpolate
output println

file write(output)
